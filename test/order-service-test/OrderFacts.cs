using System;
using System.Collections.Generic;
using order_service.Domains;
using Xunit;
using Order = order_service.Domains.Order;

namespace order_service_test
{
    public class OrderFacts : FactBase
    {
        [Fact]
        public void should_create_order_success_less_than_10_items()
        {
            var orderItem = new List<OrderItem>();
            for (var i = 0; i < 10; i++)
            {
                orderItem.Add(new OrderItem());
            }
            var order = Order.Create(orderItem);

            Assert.Equal(10, order.Items.Count);
        }
        
        [Fact]
        public void should_throw_error_when_create_order_with_more_than_10_items()
        {
            var orderItem = new List<OrderItem>();
            for (var i = 0; i < 11; i++)
            {
                orderItem.Add(new OrderItem());
            }

            Assert.Throws(typeof(ArgumentException), () => Order.Create(orderItem));
        }

        [Fact]
        public void should_lock_stock_when_create_order()
        {
            // call api
            // assert 
        }
    }
}