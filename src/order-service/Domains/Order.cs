using System;
using System.Collections.Generic;

namespace order_service.Domains
{
    public class Order
    {
        public List<OrderItem> Items { get; }

        public static Order Create(List<OrderItem> orderItem)
        {
            if (orderItem.Count > 10) throw new ArgumentException();
            return new Order(orderItem);
        }

        private Order(List<OrderItem> orderItems)
        {
            Items = new List<OrderItem>(orderItems);
        }
    }

    public class OrderItem
    {
    }
}